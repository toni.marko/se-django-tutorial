from django.shortcuts import render

# Create your views here.
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic

class SignUpView(generic.CreateView):  #klasa koju smo spemonuli u urls.py gdje tamo ona ima nakacen .as_view(),
                                       # SignUpView naslijeduje generic.CreateView klasu
                                       
    form_class = UserCreationForm      #generickoj CreateView klasi moramo reci koja form klasa ce se koristit
    success_url = reverse_lazy('login') #ako je uspjesno kreiran signup, on ga salje na navedeni url
    #!!! reverse_lazy koristimo uz class based view, a kada koristimo funkcijske vieowe koristimo obicni reverse
    
    template_name = 'registration/signup.html' #govori kako se zove template koji cemo renderirati (zove se registration/signup.html)
    #sada treba otici u base.html i dodati link na signup


