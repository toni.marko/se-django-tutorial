from django.urls import path
from . import views   #ovo from . znaci da targetiramo importanje iz trenutnog direktorija,
                      #u ovom slucaju iz views koji se nalazi u accounts direktoriju
                      
app_name = 'accounts'
urlpatterns = [
    path('sign-up', views.SignUpView.as_view(), name='signup'), #za views.SignUpView cemo unutar accounts views napraviti genericku klasu view koja ce odradivati sign up,
                                                                #ubiti koristit cemo djangovu sign up formu, ima samo username i password.
                                                                #napravit cemo template koji ce se renderirat u kojem ce bit forma, koju cemo staviti u template koji napravimo
                                                                #a SignupView cemo napraviti u views i reci mu koji je template koji koristimo
                                                                # ovaj .as_view() smo dodali na genericku klasu, kako bi se onda ona ponasala kao FUNKCIJSKI VIEW jer
                                                                # django po defaultu koristi funkcijski view
]