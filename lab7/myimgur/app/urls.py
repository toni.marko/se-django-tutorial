from django.urls import path

from . import views

app_name = 'app'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:image_id>/', views.detail, name='detail'),
    path('<int:image_id>/comment', views.comment, name='comment'),
    path('new', views.create_image, name="create_image"),  #ruta za kreiranje slika, path ce biti localhost8000/images/new
                                                           #otvorit ce html stranicu di ce bit forma
                                                           #sada moramo jos u views.py napravit akciju create image
]
