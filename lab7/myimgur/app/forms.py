from .models import Image, Comment
from django.forms import ModelForm

class ImageForm(ModelForm):  #klasa ImageForm naslijeduje ModelForm, ModelForm moze formu izgenerirat iz instance, post requesta ili iz oboje
    class Meta:
        model=Image
        fields= ['title', 'url', 'description', 'pub_date']