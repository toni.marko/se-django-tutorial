"""imgur URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

#ovdje definiramo koje su glavne rute

from django.contrib import admin
from django.urls import include,path
from app import views

urlpatterns = [                             #zapamti, rute se slijedno testiraju, pa mozemo imati vise ruta i kada pronade match izlazi van
                                            #zato specificnije rute idu gore, a generalnije dolje. Zato se '' path stavlja na kraj
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),  #dodano iznad ovog reda sto je sad ispod, da prvo proba njeg naci
    path('accounts/', include('django.contrib.auth.urls')), #dodali da bi omogucili login i signup, 
                                                            #trebamo jos za ovo dodati novi direktorij u templates naziva registration
    path('images/', include('app.urls')),
    path('', views.index, name='index'), 
]
